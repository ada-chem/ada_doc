# Ada core and environment variables

The `ada_core` package handles private variables by expecting the user
to set envrionment variables which have the appropriate value associated
with them. There are a variety of parameters that may be specified
depending on what the user is trying to accomplish. To make life easier
for the end user, `ada_core` uses the [python-dotenv](https://github.com/theskumar/python-dotenv)
package to temporarily set environment variables. When an part of
`ada_core` is imported, it will automatically search the current
working directory for any files ending in `.env`, and will temporarily
add their contents to the environment variables.

Environment variables currently used by `ada_core`:

|Variable Name|Description|
|---|---|
|BOTNAME|The name of a Slack bot used by the `ada_core.dependencies.slackbot.SlackBot` class e.g. "toothless"|
|SLACKCHANNEL|The name of a Slack channel that the above bot should use for communication. (Note that the bot must be added to the channel members for it to be able to post in the channel.)|
|TOKEN|The Slack API token specific to that bot integration|
|CHEMOSPATH|The path to a directory shared with a ChemOS istance (see chemos_communication for more details)|
|DBPATH|Path to a dropbox directory, typically this is for conveniently saving images or data to a shared folder|
|HPLCFOLDER|Path to a folder containing HPLC data (for Agilent HPLC integrations)|
|IMAGEFOLDER|Path to a folder where imaging scripts can save their images to|

Pre-configured `.env` files may be found in the [ada_private_env](https://gitlab.com/ada-chem/ada_private_env)
repository. These files should be manually downloaded from the repository
and saved in the root directory of the working project on a computer.
**Do not upload these files to the repositories, these files contain
private information which should not be distributed beyond UBC**.
`.gitignore` entries have been added to the other repositories
in `ada_chem` so that users will not accidentally upload `.env` files to
the shared repositories.

The `slack_users.env` file is for storing user identifiers for `SlackBot`
direct messaging or tagging. Additional users may be added in the
`{username}={ID}` format to this file. You can retrieve a user's ID by
opening their profile in Slack, clicking '...', then 'Copy member ID'.
Once a username/ID pair is added to this file, that user may be selected
on `SlackBot` instantation as the `user` keyword argument (e.g.
`SlackBot(user='lars')`).