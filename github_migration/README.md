# ADA Project Documentation

This project contains all general documentation for the ADA project that is not specific to a project.

## Migrating the old `nrobotics` repo to the new `ada_core` repo

The main development of the core library has been moved to the [ada_core project](https://gitlab.com/ada-chem/ada_core) 
on Gitlab, so anybody working currently working on the project will have to migrate their repository in PyCharm:

1. Open the `nrobotics` project in PyCharm
1. Select `VCS > Git > Remotes...` from the dropdown menu
<br>![Remotes](nrobotics_migration/0_remotes.png)
1. Select the `origin` remote, and click the edit pencil button
<br>![Editing Origin](nrobotics_migration/1_origin.png)
1. Change `URL` to new Gitlab URL: `https://gitlab.com/ada-chem/ada_core` and click `OK`
<br>![Origin URL](nrobotics_migration/2_origin.png)
1. You may need to enter your Gitlab credentials in the login popup window. If you have two factor authentication enabled for you account, you need to generate an access token (on GitLab under `Settings > Access Tokens`) and use that as your password. 
<br>![Login](nrobotics_migration/credentials.PNG)
1. Right-click on `nrobotics` project folder in left sidebar
1. Select `Refactor > Rename`
<br>![Rename Project](nrobotics_migration/3_rename.png)
1. Select `Rename Project` and click `OK`
<br>![rename](nrobotics_migration/4_rename.png)
1. Enter `ada_core` as the new project name and click `OK`
<br>![ada_core](nrobotics_migration/5_rename.png)
1. Right-click on `nrobotics` project folder in left sidebar
1. Select `Refactor > Rename`
1. Select `Rename Directory` and click `OK`
<br>![rename](nrobotics_migration/6_rename_dir.png)
1. Enter `ada_core` as the directory name and uncheck `Search for references` and `Search in comments and strings`, then click `Refactor`
<br>![rename](nrobotics_migration/7_rename.png)