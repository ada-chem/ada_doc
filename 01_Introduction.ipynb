{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Introduction to `ada_core`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `ada_core` package is a collection of modules, methods, and classes which can be used to interact with a \n",
    "[North Robotics](http://www.northrobotics.com/) C9 controller or N9 robot.  While each class and method in the `ada_core` package has \n",
    "documentation detailing its use and function, this documentation will tie the package together as a whole from the perspective of a user \n",
    "first approaching the package. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Connecting to a C9 controller"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `Communicator` class connects directly to a C9 controller via serial communication. All commands to the controller and anything \n",
    "connected to the controller are routed through the `controller` instace. A `Communicator` class instance (`controller`) is automatically created, and is the item which should be imported. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ada_core.dependencies.interface import controller"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Raw commands may be sent to the C9 using this instance, but it is highly recommended to let the classes and items (detailed later) solely communicate with the C9. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'ECHOSUCKA'"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "controller.execute('echo')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interacting with the robot arm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `RobotArm` class is the class which controls all things related to the arm (e.g. movement, calculations, and positions). Similar to the `controller`, a `robotarm` class instance is automatically instantiated, and should be imported directly. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ada_core.dependencies.arm import robotarm as n9"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "RobotArm(0, 0, 0, 0)"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "n9"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Basic Arm movement\n",
    "There are a variety of motion-related methods defined the `RobotArm` class, such as: \n",
    "\n",
    "- Moving to a cartesian (x, y, z) location\n",
    "- Moving indidual axes (normally only for debugging)\n",
    "- Aligning the gripper \n",
    "- Spinning the gripper\n",
    "- Keyboard driving mode\n",
    "\n",
    "For example, to move the arm to a specific cartesian location, use `move_to_location()`. The first parameter is expected to be a location (either an xyz point or a \n",
    "grid reference). Cartesian coordinates are relative to the center of the shoulder in the `xy` plane, and relative to the deck in the `z` direction. Grid references are alphanumeric references corresponding to the labelled grid reference visible on the robot deck. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "n9.move_to_location({'x': 150., 'y': 150.})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The location of a module (e.g. the gripper) can be retrieved using the `module_location()` method. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Location({'x': 149.994 mm, 'y': 150 mm, 'z': 284.8 mm})"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "n9.module_location() "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If values for any axes are omitted, the current location of the module is retrieved. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Error sending message to N9 simulator\n"
     ]
    }
   ],
   "source": [
    "n9.move_to_location({'x': 170.})  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Location({'x': 169.995 mm, 'y': 150.005 mm, 'z': 284.8 mm})"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "n9.module_location()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Typically, there are two modules installed on an N9 arm: the `gripper` (the pneumatic gripper on the bottom of the arm), and the `dispenser` (the liquid handling probe on the end of the arm). By default, the gripper is module moved in a `move_to_location()` call, but the module target may be explicitly specified using the `target` keyword argument. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "n9.move_to_location({'x': 150., 'y': 150.}, target='dispenser')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Location({'x': 151.888 mm, 'y': 108.545 mm, 'z': 284.8 mm})"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "n9.module_location()  # the gripper location"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Location({'x': 149.99 mm, 'y': 150.002 mm, 'z': 298.8 mm})"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "n9.module_location('dispenser')  # the dispenser location"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default, cartesian movement is split into `xy` and `z` components. In other words, if a location containing `x`/`y` positions as well as a `z` position is supplied, the arm will first move in the `xy` plane, finish its movement, then move in the `z` direction. This is a straightforward method of avoiding unintended \"judo chop!\" motions of the arm, which typically result in collisions. This behaviour may be tweaked using the `splitxyz` keyword argument (see the method documentation for further details). \n",
    "\n",
    "Additionally, the `safeheight()` method moves the robot arm upwards so that the target is above the specified height (relative to the deck). If the target module is already at or above the specified height, no movement will occurr. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "n9.move_to_location({'z': 100.})  # move the gripper to a height of 100 mm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Location({'x': 151.888 mm, 'y': 108.545 mm, 'z': 100 mm})"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "n9.module_location()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "n9.safeheight(150.)  # raise the gripper to a height of 150 mm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Location({'x': 151.888 mm, 'y': 108.545 mm, 'z': 150 mm})"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "n9.module_location()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "n9.safeheight(180., target='dispenser')  # raise the gripper to a height of 180 mm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Location({'x': 149.99 mm, 'y': 150.002 mm, 'z': 180 mm})"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "n9.module_location('dispenser')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Advanced arm movement\n",
    "\n",
    "There are a variety of other arm movements, such as: \n",
    "\n",
    "- Moving indidual axes (normally only for debugging)\n",
    "- Aligning the gripper \n",
    "- Spinning the gripper\n",
    "- Keyboard driving mode\n",
    "\n",
    "The `move_axes()` method of the `RobotArm` class is generally used for debugging purposes only, as it moves joints to specific positions in units of encoder counts. These are directly proportional to cartesian positions, but there are a variety of convenience methods built into the `RobotArm` class which allow the user to avoid thinking in counts. \n",
    "\n",
    "The gripper may be aligned to an axis on the robot deck, or to a specified number of degrees relative to the forearm using the `align_gripper()` method. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "n9.align_gripper('x')  # align to the x-axis of the deck"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [],
   "source": [
    "n9.align_gripper('y')  # align to the y-axis of the deck"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "n9.align_gripper(45.)  # align to 45 degrees clockwise relative to the forearm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The gripper may be spun at a specified revolutions per minute and duration (seconds) using the `spin_gripper()` method. This is most commonly used for agitating solutions in containers held in the gripper. \n",
    "\n",
    "Finally, the arm may be driven by the keyboard using the `keyboard()` method. See the documentation of that method for further details. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
