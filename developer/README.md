`git` Workflows
===============

The `ada_core` development workflow is more-or-less a copy of the 
[GitLab Flow workflow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html). Most of the development is coordinated 
using the `master` branch, and each week a new release branch will be made with a tested release. New branches will 
be branched off of `master` for bug fixes and new features and will be merged back into the `master` branch using 
GitLab's merge request feature.

## Release Schedule
|Day|Description|
|---|---|
|Monday|Testing|
|Tuesday|Release|
|Friday|Feature Freeze|

## Branches

|Name|Description|
|----|-----------|
|master|Main branch used to coordinate releases, this will always contain the latest "unstable" version of the code|
|stable|Contains the latest stable release|
|vX.Y.Z|Each weekly release will have a branch named after the release version number (format detailed below)|

## `vX.Y.Z` Version Naming Scheme
The `ada_core` version naming scheme is based on the [Semantic Versioning](https://semver.org/) naming scheme. This 
scheme contains three numbers:

|type|description|
|---|---|
|X|"Major" version number, currently 0, which will be incremented once `ada_core` is mature and stable|
|Y|"Minor" version number, incremented for a weekly release|
|Z|"Patch" version number, reset for each "minor" release, incremented each time a bug is fixed| 

Working On a New Feature
========================
1. Create a new issue and merge request
    1. Navigate to [Issues Page](https://gitlab.com/ada-chem/ada_core/issues) (Issues > List in sidebar)
    1. Create a new issue (New issue button in top right)
    1. Add a short, descriptive title (ideally 4-8 words)
    1. Add a longer description describing the new feature
    1. Assign the issue to yourself (Assign to me link)
    1. Select the "Feature" label
    1. Add a weight to the issue (see **Weighting** below)
    1. Submit issue
    1. Click "Create merge request"

1. Checkout new branch in PyCharm
    1. Run VCS > Git > Fetch
    1. Open VCS > Git > Branches...
    1. Find and click on your new branch in the "Remote branches" list
    1. Select "Checkout as..." and click OK to accept the default name

1. Work on your feature
    1. Do your best to finish development within a week to coordinate with a release
    1. If you need to merge in bug fixes, run "VCS > Git > Pull..." to pull the latest changes from the `master` branch
    1. Add comments to your merge request to discuss any questions related to the code

1. Test your new feature
    1. Test your bug fix in the simulator and on a real robot

1. Push and review your changes
    1. Run "VCS > Git > Push" to push your latest changes to GitLab
    1. Find the merge request you created in the 
    [merge requests list](https://gitlab.com/ada-chem/ada_core/merge_requests) 
    1. Click "Edit"
    1. Click "Remove the WIP: prefix" link
    1. Assign Sean (@v13inc) or Lars (@larsyunker) as an approver
    1. Save changes

Fixing a Bug
============
### Steps to take when troubleshooting:
1. If you encounter movement or values which don’t match what you’d
    expect, try to trace back the problem to its source line in your
    script.
    - There will be some point where things start going wrong, and can
        usually be traced back to a single line.
    - This may require some time. The debug mode of PyCharm can be
        extremely helpful here, as you can introduce stop points in your
        script to evaluate variable values or robot position.
        Alternatively, you can introduce print statements into your
        script, but these are difficult to keep track of, and they all
        need to be removed once you finish debugging.
1. Once you’ve found the offending line in your script, follow the
    erroneous value through the methods or operations which modify it,
    and try to establish what went wrong.
1. If you find yourself deep in the `ada_core` library, consider
    bringing Sean or Lars into the troubleshooting process.
    - The low-level `ada_core` methods and classes are structured in
        very specific ways, and great care must be taken with any change
        at this level.
    - If you find yourself at the point of wanting to interact with or
        modify a method or variable preceeded by an `_`, bring Lars or
        Sean in. These are “protected” attributes, and are never
        intended to be modified or interacted with at the user level.

### How to start fixing a bug:
1. If there is no issue describing a bug, create a new Issue entry on
    GitLab using the same method described above for creating an issue.
    For bugs, select the "Bug" label instead of the "Feature" label.
1. Create a new merge request for the bug fix
    1. Navigate to the issue page for the bug in Gitlab
    1. Click "Create merge request"

1. Checkout new branch in PyCharm
    1. Run VCS > Git > Fetch
    1. Open VCS > Git > Branches...
    1. Find and click on your new branch in the "Remote branches" list
    1. Select "Checkout as..." and click OK to accept the default name
 
1. Work on your bug fix
    1. Do your best to finish development within a week to coordinate with a release
    1. Add comments to your merge request to discuss any questions related to the code

1. Test your bug fix
    1. Test your bug fix in the simulator and on a real robot

1. Push and review your changes
    1. Run "VCS > Git > Push" to push your latest changes to GitLab
    1. Find the merge request you created in the 
    [merge requests list](https://gitlab.com/ada-chem/ada_core/merge_requests) 
    1. Click "Edit"
    1. Click "Remove the WIP: prefix" link
    1. Assign Sean (@v13inc) or Lars (@larsyunker) as an approver
    1. Save changes


Issue/Feature Weighting
=======================
When you create a new issue or feature request, apply a weighting to it
to signify to other users how important that bug/feature is. Use the
following table as a guideline:

|Weight|Type|Description|
|----|-----------|---|
|0|Critical Bug|This is a bug that affects the core functionality of `ada_core`, and must be addressed immediately.|
|1|Bug|This is a bug that affects a number of `ada_core` functionalities, but can be worked around. This type of bug should be fixed as soon as possible.|
|2|Minor Bug|This is a bug that affects edge cases, can be worked around for the time being, but should be addressed soon|
|3|Required Feature|This is a feature that `ada_core` will need in the near future (people currently have to work around the absence of this feature to continue working on their current project.|
|4|Feature|This is a feature that would be nice to have, but current projects are not waiting for this feature to move forward.|

Documentation Guidelines
========================
Your script should conform to this guide prior to any commits, and must
 conform to this prior to merges.

### Docstrings
The docstring is the part of a method which helps the user understand
what the purpose, expectations, and return of a method are. PyCharm has
a very convenient way of auto-populating the necessities of a docstring.
To use the auto-create feature:
1. Define your signature of your method, with all necessary variables.
    e.g.
    ```
    def new_demonstration_method(arg1, arg2, kwarg1=True):
    ```
1. Directly after the `:` of the method definition, press Enter.
1. Type three quotations (`"""`), which will automatically pair quotations
    to form `""""""`
1. With the cursor at the center of the qotations (`"""|"""`), hit Enter
    again, which will auto-populate the docstring to the required
    format. e.g.
    ```
    def new_demonstration_method(arg1, arg2, kwarg1=True):
        """

        :param arg1:
        :param arg2:
        :param kwarg1:
        :return:
        """
    ```
1. Fill in the docstring using the guidelines below. To complete the
    example, this would be a completed docstring.
    ```
    def new_demonstration_method(arg1, arg2, kwarg1=True):
        """
        This is a method used to describe how to create a docstring
        automatically using PyCharm.

        :param int arg1: the first example argument, which in this case
            is an integer. Note the type hint between "param" and "arg1".
            This will be interpreted in formatted documentation as the
            expected type of the argument.
        :param str arg2: the second example argument, which in this
            case is a string. Note how in multi-line argument descriptions,
            the extra lines are indented, which associates the lines
            with the previous argument.
        :param bool kwarg1: A keyword argument used by this method.
        :return: Description of the return of this example method. The
            type of the return value is defined by the special :rtype:.
        :rtype: bool
        """
    ```


The docstring of every method and class in `ada_core` is required to have
the following:
- A detailed description of what the method does
- A detailed description for every argument and keyword argument that
    the function/class accepts (including type hints for each)
- A detailed description of the method return (please also define
    `:rtype:`)
- If the method changes the state of the robot, component, or item:
    - Clearly define the expected state of the robot/component/item
        before the method
    - Clearly describe what physically happens during the method
        (this is best described in the detailed method description)
    - Clearly define the resulting state of the robot/component/item
        upon completion of the method.

### In-code commenting
In-code commenting should include, wherever possible:
- A comment for each code block (a few lines that accomplish a specific
    task)
- Good rules of thumb: the user should be able to follow your script
    without having to read and interpret every line. Take care though,
    this project will outlive you, and if no one is able to follow your
    script, your script will fall by the wayside and not be used. Think of this like your lab notebook: someone should be able to come in and use your experimental procedure to exactly recreate what you did.
- If a method call is descriptively named, a comment is probably unnecessary
- While variables should be descriptively named, an additional comment
    describing that variable’s purpose is required

### Style guide:
Follow PEP-8 wherever possible. A good guide on how to use PEP-8 can
be found [at this link that I will add later]. In particular, please
closely follow these guidelines:

- If you’re using PyCharm, it will warn you if your code does not follow
    a PEP-8 or Python convention, follow the instructions of PyCharm to
    correct infractions
- Descriptively name your variables, methods, and classes
    - The user should be able to intuit what something is by its name
    - Don’t go overboard, naming a method `sequence_push` is too short
        and not helpful in understanding, where
        `sequence_to_push_sample_plug_to_hplc` is overly verbose.
        Something like `push_to_hplc` would be more appropriate here.
    - Similarly, variables should also be named in the
        lowercase-underscore style, and should give a good idea of
        what that variable is storing. e.g. a variable named “volume”
        is not descriptive enough, “volume_contained_in_syringe” is
        overly verbose, and “contained_volume” is just right.
- For function and method definition lines:
    - If a variable needs to be defined and modifiable by the user,
        but is not regularly modified (i.e. a default value serves for
        most purposes), it should be a keyword argument. For example,
        the robotarm armbias attribute is set to a default
        ‘min shoulder change’  on instantiation, which serves for most
        applications, but the user could change that value should they
        choose to for their script’s use of that class.
    - If a variable must be set by the user on every call (e.g.
        specifying a volume for starting a pump motion), it should be
        an argument for that method

